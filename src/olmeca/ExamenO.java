/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olmeca;

import Conexion.Conectar;
import ExamenOlmeca.PreguntasO;
import ExamenOlmeca.RespuestasO;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.ImageIcon;
import javax.swing.Timer;

/**
 *
 * @author RolandoG  
 */
public class ExamenO extends javax.swing.JFrame {

    int calificacion;
    int resultado=0;
    int respMalas = 0;
    String usuario;
    
    private Timer t;
    private ActionListener al;
    /**
     * Creates new form ExamenO
     */
    public ExamenO(String usuario) {
        initComponents();
        ImageIcon inc=new ImageIcon (getClass().getResource("/Iconos/pyramid.png"));
        Image image=inc.getImage();
        this.setIconImage(image);
        this.usuario=usuario;
        String userBD= usuario;
        usuarioTxt.setText(userBD);
        preguntaL.setText(pregunta.getPregunta(posicion));
        
        String[] a = r.setRespuestas(posicion);
            
            // boton que agrupa lo checkBox
             // borrar las selecciones de los RadioButton
            buttonGroup1.clearSelection();
            
            // asigna en la posicion a las posible respueta
            opc1.setText(a[0]);
            opc2.setText(a[1]);
            opc3.setText(a[2]);
            
        opc1.requestFocus();
        backBt.setEnabled(false);
        finalizarBt.setEnabled(false);
        
    }
    
    public ExamenO() {
      initComponents();  
    }
    
    int posicion = 0;
    RespuestasO r = new RespuestasO();
    PreguntasO pregunta = new PreguntasO ();
    Object[] select = {"","","","","","","","","",""};
    
    // base de datos
    Conectar cc = new Conectar();
    Connection cn = cc.conexion();
    
    Long idbd=null;
    
    
    void avanzar(){
        if(posicion==8){
            nextBt.setEnabled(false);
            finalizarBt.setEnabled(true);
        }
        
        if(posicion <9){
            backBt.setEnabled(true);
            // avanza a la pregunta siguiente 
            posicion++;
            preguntaL.setText(pregunta.getPregunta(posicion));

            // a es igual a un auxiliar para asignar la respuestas
            String[] a = r.setRespuestas(posicion);

            // boton que agrupa lo checkBox
            // borrar las selecciones de los RadioButton
            buttonGroup1.clearSelection();

            // asigna en la posicion a las posible respueta
            opc1.setText(a[0]);
            opc2.setText(a[1]);
            opc3.setText(a[2]);
            opc1.requestFocus();
        }else{
            Toolkit.getDefaultToolkit().beep();
        }
        // mientras va cambiando las preguntas 
        // la barra va aumentando la barra de progreso 
        avanceP.setValue(avanceP.getValue()+11);
    }
    void resultado(){
        calificacion = 0; // guarda resultado calificacion
        int buenas=0;
        int malas = 0;

        // inicia un for en cual
        // y guarda las respuestas buenas en la variable buenas
        // y guarda las respuestas buenas en calificacion
        for(int i =0; i <10; i++){
            if(select[i].equals(r.getRespuesta(i))){ // se comparan las respuestas
                calificacion = calificacion + 1;
                buenas = buenas + 1; // aumenta los aciertos buenos

                // si las respuestas son diferentes se consideran que estan mal
                // y las guarda en la variabel malas
            }else{
                if(!select[i].equals(r.getRespuesta(i))){
                    // si la respuesta es erronea se guarda en un contador
                    malas = malas + 1;
                }
            }
        }

        

        // la calificacion la guardamos en la variable resultado
        resultado = calificacion;
        respMalas = malas;
        backBt.setEnabled(false);

        // INSERTA EL RESULTADO OBTENIDO EN CADA QUIZZ
   
        try{

             PreparedStatement pst = cn.prepareStatement("INSERT INTO puntaje "
                + "(calificacion,aciertosbuenos,errores)"
                + " values(?,?,?)");
            pst.setInt(1, resultado); // calificacion en MySQL
            pst.setInt(2, buenas);
            pst.setInt(3, respMalas);

            pst.executeUpdate();

        }catch(Exception e ){
        }
        
        String ultimoVal=null;
        try{
            PreparedStatement pst = cn.prepareStatement("Select * from puntaje order by codigo desc");
            ResultSet rs = pst.executeQuery();
            
            if(rs.next()){
                ultimoVal = rs.getString("codigo");
            }
            pst.executeUpdate();
        }catch(Exception e){
            
        }
        try{
            PreparedStatement pst = cn.prepareStatement("INSERT INTO calificacion"
                + "(usuario,codigo)"
                + " values(?,?)");
            pst.setString(1,usuarioTxt.getText()); // calificacion en MySQL
            pst.setString(2, ultimoVal);
            

            pst.executeUpdate();
        }catch(Exception e){
            
        }
        Resultado r= new Resultado(resultado);
        r.setVisible(true);
        r.setLocationRelativeTo(null);
        hide();

    }
    
    void retroceder(){
       if(posicion==1){
            backBt.setEnabled(false);
        }
        // cundo la posicion es diferente de 3
        // abilita el boton avanzar
        // y deshabilita el boton finalizar
        if(posicion!=10){
            nextBt.setEnabled(true);
            finalizarBt.setEnabled(false);
        }

        if(posicion > -1){
            posicion --;

            preguntaL.setText(pregunta.getPregunta(posicion));

            // a es igual a un auxiliar para asignar la respuestas
            String[] a = r.setRespuestas(posicion);

            // boton que agrupa lo checkBox
            // borrar las selecciones de los RadioButton
            buttonGroup1.clearSelection();

            // asigna en la posicion a las posible respueta
            opc1.setText(a[0]);
            opc2.setText(a[1]);
            opc3.setText(a[2]);
            

            opc1.requestFocus();
        }else{
            Toolkit.getDefaultToolkit().beep();
        }
        
        // muestra las barra de progreso mientras 
        // va retrocediendo disminuye la barra
        avanceP.setValue(avanceP.getValue()-11); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        usuarioTxt = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        preguntaL = new javax.swing.JLabel();
        opc1 = new javax.swing.JRadioButton();
        opc2 = new javax.swing.JRadioButton();
        opc3 = new javax.swing.JRadioButton();
        backBt = new javax.swing.JButton();
        nextBt = new javax.swing.JButton();
        finalizarBt = new javax.swing.JButton();
        avanceP = new javax.swing.JProgressBar();
        jButton1 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setSize(new java.awt.Dimension(716, 451));

        jPanel1.setBackground(new java.awt.Color(204, 153, 0));

        usuarioTxt.setForeground(new java.awt.Color(204, 153, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Ponte a prueba");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(132, 132, 132)
                .addComponent(usuarioTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 63, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(usuarioTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(19, 19, 19))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        preguntaL.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N

        opc1.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(opc1);
        opc1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc1ActionPerformed(evt);
            }
        });

        opc2.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(opc2);
        opc2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc2ActionPerformed(evt);
            }
        });

        opc3.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(opc3);
        opc3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opc3ActionPerformed(evt);
            }
        });

        backBt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/back (4).png"))); // NOI18N
        backBt.setBorder(null);
        backBt.setBorderPainted(false);
        backBt.setContentAreaFilled(false);
        backBt.setDefaultCapable(false);
        backBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtActionPerformed(evt);
            }
        });

        nextBt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/next (2).png"))); // NOI18N
        nextBt.setBorder(null);
        nextBt.setBorderPainted(false);
        nextBt.setContentAreaFilled(false);
        nextBt.setDefaultCapable(false);
        nextBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nextBtActionPerformed(evt);
            }
        });

        finalizarBt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ExamenOlmeca/trophy.png"))); // NOI18N
        finalizarBt.setBorder(null);
        finalizarBt.setBorderPainted(false);
        finalizarBt.setContentAreaFilled(false);
        finalizarBt.setDefaultCapable(false);
        finalizarBt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finalizarBtActionPerformed(evt);
            }
        });

        avanceP.setBackground(new java.awt.Color(255, 255, 255));
        avanceP.setForeground(new java.awt.Color(255, 204, 153));
        avanceP.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        avanceP.setBorderPainted(false);
        avanceP.setFocusable(false);

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/menu.png"))); // NOI18N
        jButton1.setBorder(null);
        jButton1.setBorderPainted(false);
        jButton1.setContentAreaFilled(false);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Rockwell", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 153, 153));
        jLabel3.setText("Menú principal");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opc3)
                            .addComponent(opc2)
                            .addComponent(opc1)
                            .addComponent(preguntaL, javax.swing.GroupLayout.PREFERRED_SIZE, 575, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(99, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(nextBt, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(avanceP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(50, 50, 50))))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel3))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1)
                            .addComponent(backBt, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(240, 240, 240)
                        .addComponent(finalizarBt, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(avanceP, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(preguntaL, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(opc1)
                .addGap(18, 18, 18)
                .addComponent(opc2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addComponent(opc3)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(finalizarBt, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(backBt, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(nextBt, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void opc1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc1ActionPerformed
        // TODO add your handling code here:
        select[posicion] = opc1.getLabel();
        
    }//GEN-LAST:event_opc1ActionPerformed

    private void backBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtActionPerformed
        // TODO add your handling code here:
        retroceder();
        
    }//GEN-LAST:event_backBtActionPerformed

    private void nextBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nextBtActionPerformed
        // TODO add your handling code here:
        avanzar();
                 
    }//GEN-LAST:event_nextBtActionPerformed

    private void opc2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc2ActionPerformed
        // TODO add your handling code here:
        select[posicion] = opc2.getLabel();
    }//GEN-LAST:event_opc2ActionPerformed

    private void opc3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opc3ActionPerformed
        // TODO add your handling code here:
        select[posicion] = opc3.getLabel();
    }//GEN-LAST:event_opc3ActionPerformed

    private void finalizarBtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finalizarBtActionPerformed
        // TODO add your handling code here:
        resultado();
        
    }//GEN-LAST:event_finalizarBtActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        MenuInicio m= new MenuInicio();
        m.setVisible(true);
        m.setLocationRelativeTo(null);
        hide();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ExamenO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ExamenO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ExamenO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ExamenO.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ExamenO().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar avanceP;
    private javax.swing.JButton backBt;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton finalizarBt;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton nextBt;
    private javax.swing.JRadioButton opc1;
    private javax.swing.JRadioButton opc2;
    private javax.swing.JRadioButton opc3;
    private javax.swing.JLabel preguntaL;
    private javax.swing.JLabel usuarioTxt;
    // End of variables declaration//GEN-END:variables
}
