/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olmeca;


/**
 *
 * @author tito
 */

/**
 * Clase principal de todo el programa
 * @author tito
 */
public class Olmeca {

    
    /**
     * Método para iniciar la primera pantalla del programa
     * @param args 
     */
    public static void main(String[] args) {
     
        PantallaPrincipal inicio = new PantallaPrincipal();
        inicio.setLocationRelativeTo(null);
        inicio.setVisible(true);
    }
    
}
