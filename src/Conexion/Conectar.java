/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author RolandoG
 */
public class Conectar {
    Connection conn = null;
    
    public Connection conexion(){
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("Drive manager registrado");
            
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/olmecap?" + 
             "useSSL=false&serverTimezone=UTC&user=root&password=Rolajose13");
            System.out.println("Conexión establecida");
            
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return conn;
    }  
}
