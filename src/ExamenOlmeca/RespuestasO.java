/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ExamenOlmeca;

import java.util.StringTokenizer;

/**
 *
 * @author RolandoG
 */
public class RespuestasO {
    String[] respuestas = {
        "Veracruz", "Jaguar", "La Venta", 
        "San Lorenzo", "Sierra de los Tuxtlas", "Los Olmecas","Mesoamerica",
        "Agricultura", "Falso", "Cierto"
        
    };
    
    String[] radioR = {
       "Veracruz,Xalapa,Tabasco",
       "Lobo,Perro,Jaguar",
       "Los Tecajetes,La Venta,El Zamoral",
       "La Antigua,Quiahuixtlán,San Lorenzo",
       "Sierra Madre Occidental,Sierra de los Tuxtlas,Sierra Madre Oriental", 
       "Huastecos,Totonacas,Los Olmecas",
       "Mesoamerica,Mesopotamia,Región de los Andes",
       "Conquista,Agricultura,Pintura", 
       "Cierto,Falso,Me da igual",
       "Cierto,Falso,Me da igual"

       
    };
    
    public String getRespuesta(int posicion){
        return respuestas[posicion];
    }
    
    public String[] separar(String cadena, String separador){
        StringTokenizer token = new StringTokenizer(cadena,separador);
        
        // a = auxiliar
        
        String [] a = new String[4];
        
        // movernos en el arreglo
        int i = 0;
        
        while(token.hasMoreTokens()){
            a[i] = token.nextToken(); // obtenemos el token de la posicion
            i++;
        }
        
        return a;
    }
    
    public String[] setRespuestas(int posicion){
        String s1 = radioR[posicion];
        String[] s2 = separar(s1,",");
        return s2;
    }
    
}
